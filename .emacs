;; Paths
(setq load-path (cons "~/emacs/" load-path))
(setq load-path (cons "~/emacs/themes/" load-path))
(setq load-path (cons "~/emacs/modes/" load-path))

;; Uniquify
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Color Theme
(require 'color-theme)
(setq color-theme-is-global t)
(color-theme-initialize)
;;(color-theme-blue-gnus)
(color-theme-spartan)

;;(set-background-color "#000000")
;;(set-foreground-color "#FFFFFF")
;;(set-cursor-color "#00FF00")
;;(set-face-background 'region "darkblue")
;;(set-face-foreground 'region "grey")

;; Font
;; In linux use xlsfonts to list available fonts
;; (set-default-font "-adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1")
;;(set-default-font "-adobe-courier-medium-r-normal--14-140-75-75-m-90-iso10646-1")

;; Templates
(require 'template)
(template-initialize)

;; rust
(load "rust-mode.el")
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
(require 'rust-mode)

;; js2
(autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))

;; python
(add-to-list 'auto-mode-alist '("\\.wsgi$" . python-mode))
(add-hook 'python-mode-hook '(lambda () (define-key python-mode-map "\C-m" 'newline-and-indent)))

;; Loads Erlang mode
(load "erlang.el" nil t t)
(add-to-list 'auto-mode-alist '("\\.erl$" . erlang-mode))

;; ;; Loads ReST mode
;; (setq frame-background-mode 'dark)
;; (require 'rst)
;; (setq auto-mode-alist
;;       (append '(("\\.txt$" . rst-mode)
;;                 ("\\.rst$" . rst-mode)
;;                 ("\\.rest$" . rst-mode)) auto-mode-alist))

;; Disable Startup Message
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq initial-scratch-message nil)

;; Selection highlight
(delete-selection-mode t)

;; Show line numbers
(line-number-mode t)

;; Show column number
(column-number-mode t)

;; Show matching parens
(show-paren-mode t)

;; Show current function
(which-func-mode t)

;; Auto-complete
(icomplete-mode t)

;; Paren highlight
(show-paren-mode t)

;; Remove toolbar
(tool-bar-mode 0)

;; Remove menu
;; (menu-bar-mode 0)

;; Remove scroll bar
(scroll-bar-mode nil)

;; Shell colors
(ansi-color-for-comint-mode-on)

;; Mouse avoidance
;;(mouse-avoidance-mode "animate")

;; Use spaces, not tabs
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; Ispell program
(setq-default ispell-program-name "aspell")

;; No backup files
(setq make-backup-files nil)

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;; Global font-lock-mode
(global-font-lock-mode t)

;; Undo
(global-set-key "\C-x\p" 'undo)

;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; CC-mode setup
(defun my-c-mode-common-hook ()
  ;; my customizations for all of c-mode, c++-mode, and objc-mode
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'case-label 1)
  (c-set-offset 'defun-block-intro 4)
  (c-set-offset 'access-label -2)
  (setq c-recognize-knr-p nil)
  (setq c-basic-offset 4)
  (setq-default indent-tabs-mode nil)
  ;; Set the return key to autoindent
  (local-set-key "\C-m" 'reindent-then-newline-and-indent)
)
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; Detection modes
;; (add-to-list 'auto-mode-alist '("\\.html$" . html-mode))
;; (add-to-list 'auto-mode-alist '("\\.tpl$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.sql$" . sql-mode))
(add-to-list 'auto-mode-alist '("\\.xsd$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.dtd$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.xml$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.css$" . css-mode))

;; (add-hook 'html-mode-hook
;;           (lambda ()
;;             ;; Default indentation is usually 2 spaces, changing to 4.
;;             (set (make-local-variable 'sgml-basic-offset) 4)))

;; Loads Django mode
;; (load "django-html-mode.el")
;; (add-to-list 'auto-mode-alist '("\\.[sx]?html?\\'" . django-html-mode))

;; Loads Mapserver mode
(autoload 'mapserver-mode "mapserver-mode" "Mode for editing UMN MapServer files." t)
(add-to-list 'auto-mode-alist '("\\.map\\'" . mapserver-mode))

;; offset
(setq c-basic-offset 4)

;; Take any buffer and turn it into an html file, including syntax hightlighting
(require 'htmlize)

;; Make CSS colors their actual colors
(require 'cl)
(defun hexcolour-luminance (color)
  "Calculate the luminance of a color string (e.g. \"#ffaa00\", \"blue\").
  This is 0.3 red + 0.59 green + 0.11 blue and always between 0 and 255."
  (let* ((values (x-color-values color))
         (r (car values))
         (g (cadr values))
         (b (caddr values)))
    (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))

(defun hexcolour-add-to-font-lock ()
  (interactive)
  (font-lock-add-keywords nil
   `((,(concat "#[0-9a-fA-F]\\{3\\}[0-9a-fA-F]\\{3\\}?\\|"
               (regexp-opt (x-defined-colors) 'words))
      (0 (let ((colour (match-string-no-properties 0)))
           (put-text-property
            (match-beginning 0) (match-end 0)
            'face `((:foreground ,(if (> 128.0 (hexcolour-luminance colour))
                                      "white" "black"))
                    (:background ,colour)))))))))

;; Whitespaces
(setq-default show-trailing-whitespace t)
(setq-default default-indicate-empty-lines t)
(setq-default indent-tabs-mode nil)

(set-face-bold-p 'font-lock-keyword-face t)
(set-face-italic-p 'font-lock-comment-face t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(haskell-mode-hook (quote (turn-on-haskell-indent turn-on-haskell-indentation turn-on-haskell-simple-indent)))
 '(jde-jdk-registry (quote (("1.6.0.16" . "/usr/lib/jvm/java-6-sun-1.6.0.16/"))))
 '(js2-auto-indent-flag nil)
 '(js2-mirror-mode nil)
 '(js2-mode-escape-quotes nil)
 '(show-paren-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(el-get 'sync)

;; Autocomplete
(require 'auto-complete-config)
(ac-config-default)
(setq ac-show-menu-immediately-on-auto-complete t)

;;Jedi
(require 'jedi)
(add-to-list 'ac-sources 'ac-source-jedi-direct)
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Projectile
(require 'projectile)
(projectile-global-mode)

(defvar jedi-config:with-virtualenv nil
  "Set to non-nil to point to a particular virtualenv.")
;; Variables to help find the project root
(defvar jedi-config:vcs-root-sentinel ".git")
(defvar jedi-config:python-module-sentinel "__init__.py")
;; Function to find project root given a buffer
(defun get-project-root (buf repo-type init-file)
  (vc-find-root (expand-file-name (buffer-file-name buf)) repo-type))
(defvar jedi-config:find-root-function 'get-project-root)
;; And call this on initialization
(defun current-buffer-project-root ()
  (funcall jedi-config:find-root-function
           (current-buffer)
           jedi-config:vcs-root-sentinel
           jedi-config:python-module-sentinel))

(defun jedi-config:setup-server-args ()
  ;; little helper macro
  (defmacro add-args (arg-list arg-name arg-value)
    `(setq ,arg-list (append ,arg-list (list ,arg-name ,arg-value))))
  (let ((project-root (current-buffer-project-root)))
    ;; Variable for this buffer only
    (make-local-variable 'jedi:server-args)
    ;; And set our variables
    (when project-root
      ;; (setq python-environment-directory (concat project-root "env"))
      (add-args jedi:server-args "--sys-path" project-root)
      (add-args jedi:server-args "--sys-path" (concat project-root "env/lib/python2.7/site-packages"))
      ;; (add-args jedi:server-args "--virtual-env" (concat project-root "env"))
      ;; (virtualenv-activate project-root)
      )
    (when jedi-config:with-virtualenv
      (add-args jedi:server-args "--virtual-env"
                jedi-config:with-virtualenv))))

(defun jedi-config:setup-python-environment-diretory ()
  (let ((project-root (current-buffer-project-root)))
    ;; Variable for this buffer only
    (make-local-variable 'jedi:server-args)
    ;; And set our variables
    (when project-root
      (setq python-environment-directory (concat project-root "env"))
      )))

(add-hook 'python-mode-hook
          'jedi-config:setup-python-environment-diretory)
(add-hook 'python-mode-hook
          'jedi-config:setup-server-args)

;; Some Jedi keys
(defun jedi-config:setup-keys ()
  (local-set-key (kbd "M-.") 'jedi:goto-definition)
  (local-set-key (kbd "M-,") 'jedi:goto-definition-pop-marker)
  (local-set-key (kbd "M-?") 'jedi:show-doc)
  (local-set-key (kbd "M-/") 'jedi:get-in-function-call))
(add-hook 'python-mode-hook 'jedi-config:setup-keys)
(global-set-key (kbd "C-c a") 'jedi:complete)

;; Jump directly to definition
(setq jedi:goto-definition-config
      '((nil definition nil)
        (t   definition nil)
        (nil nil        nil)
        (t   nil        nil)
        (nil definition t  )
        (t   definition t  )
        (nil nil        t  )
        (t   nil        t  )))

;; Autopair
(electric-pair-mode 1)

;; Direx
(require 'direx)
(global-set-key (kbd "C-x C-j") 'direx:jump-to-directory)


;; Jedi-direx
(load "jedi-direx.el")

(eval-after-load "python-mode"
  '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
(add-hook 'jedi-mode-hook 'jedi-direx:setup)


;; Emacs IPython Notebook
(require 'ein)
(add-hook 'ein:connect-mode-hook 'ein:jedi-setup) ;; work with Jedi


;; Web mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(setq web-mode-tag-autocomplete-style 2)
(setq web-mode-markup-indent-offset 4)
(setq web-mode-css-indent-offset 4)
(setq web-mode-enable-current-element-highlight t)

;; (load "kenobi.el")

(setq ropemacs-global-prefix "C-c r")
(setq ropemacs-enable-autoimport 't)
(defun load-ropemacs ()
  "Load pymacs and ropemacs"
  (interactive)
  (require 'pymacs)
  (pymacs-load "ropemacs" "rope-")
  ;; Automatically save project python buffers before refactorings
  (setq ropemacs-confirm-saving 'nil)
)
(add-hook 'python-mode-hook 'load-ropemacs)
(global-set-key (kbd "C-c i") 'rope-auto-import)

(global-set-key (kbd "C-x g") 'magit-status)

(require 'highlight-symbol)
(highlight-symbol-mode 't)
(setq highlight-symbol-idle-delay 5)

(add-to-list 'load-path "~/emacs/modes/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq projectile-switch-project-action 'neotree-projectile-action)
