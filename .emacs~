;; Paths
(setq load-path (cons "~/emacs/" load-path))
;;(setq load-path (cons "~/emacs/themes/" load-path))
(setq load-path (cons "~/emacs/modes/" load-path))

;; Uniquify
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Color Theme
;;(require 'color-theme)
;;(setq color-theme-is-global t)
;;(color-theme-initialize)
;;(color-theme-blue-gnus)
(color-theme-dark-laptop)

;;(set-background-color "#000000")
;;(set-foreground-color "#FFFFFF")
;;(set-cursor-color "#00FF00")
;;(set-face-background 'region "darkblue")
;;(set-face-foreground 'region "grey")

;; Font
;; In linux use xlsfonts to list available fonts
;; (set-default-font "-adobe-courier-medium-r-normal--12-120-75-75-m-70-iso8859-1")
(set-default-font "-adobe-courier-medium-r-normal--14-140-75-75-m-90-iso10646-1")

;; Templates
(require 'template)
(template-initialize)

;; js2
(autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))

;; python
(add-to-list 'auto-mode-alist '("\\.wsgi$" . python-mode))
(add-hook 'python-mode-hook '(lambda () (define-key python-mode-map "\C-m" 'newline-and-indent)))

;; Loads Erlang mode
(load "erlang.el" nil t t)
(add-to-list 'auto-mode-alist '("\\.erl$" . erlang-mode))

;; Loads ReST mode
(setq frame-background-mode 'dark)
(require 'rst)
(setq auto-mode-alist
      (append '(("\\.txt$" . rst-mode)
                ("\\.rst$" . rst-mode)
                ("\\.rest$" . rst-mode)) auto-mode-alist))

;; Disable Startup Message
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq initial-scratch-message nil)

;; Selection highlight
(delete-selection-mode t)

;; Show line numbers
(line-number-mode t)

;; Show column number
(column-number-mode t)

;; Show matching parens
(show-paren-mode t)

;; Show current function
(which-func-mode t)

;; Auto-complete
(icomplete-mode t)

;; Paren highlight
(show-paren-mode t)

;; Remove toolbar
;;(tool-bar-mode 0)

;; Remove menu
(menu-bar-mode 0)

;; Remove scroll bar
(scroll-bar-mode nil)

;; Shell colors
(ansi-color-for-comint-mode-on)

;; Mouse avoidance
(mouse-avoidance-mode "animate")

;; Use spaces, not tabs
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

;; Ispell program
(setq-default ispell-program-name "aspell")

;; No backup files
(setq make-backup-files nil)

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;; Global font-lock-mode
(global-font-lock-mode t)

;; Undo
(global-set-key "\C-x\p" 'undo)

;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; CC-mode setup
(defun my-c-mode-common-hook ()
  ;; my customizations for all of c-mode, c++-mode, and objc-mode
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'case-label 1)
  (c-set-offset 'defun-block-intro 4)
  (c-set-offset 'access-label -2)
  (setq c-recognize-knr-p nil)
  (setq c-basic-offset 4)
  (setq-default indent-tabs-mode nil)
  ;; Set the return key to autoindent
  (local-set-key "\C-m" 'reindent-then-newline-and-indent)
)
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; Detection modes
(add-to-list 'auto-mode-alist '("\\.html$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.tpl$" . html-mode))
(add-to-list 'auto-mode-alist '("\\.sql$" . sql-mode))
(add-to-list 'auto-mode-alist '("\\.xsd$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.dtd$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.xml$" . xml-mode))
(add-to-list 'auto-mode-alist '("\\.css$" . css-mode))

;; Loads Django mode
(load "django-html-mode.el")
(add-to-list 'auto-mode-alist '("\\.[sx]?html?\\'" . django-html-mode))

;; Loads Mapserver mode
(autoload 'mapserver-mode "mapserver-mode" "Mode for editing UMN MapServer files." t)
(add-to-list 'auto-mode-alist '("\\.map\\'" . mapserver-mode))

;; offset
(setq c-basic-offset 4)

;; Take any buffer and turn it into an html file, including syntax hightlighting
(require 'htmlize)

;; Make CSS colors their actual colors
(require 'cl)
(defun hexcolour-luminance (color)
  "Calculate the luminance of a color string (e.g. \"#ffaa00\", \"blue\").
  This is 0.3 red + 0.59 green + 0.11 blue and always between 0 and 255."
  (let* ((values (x-color-values color))
         (r (car values))
         (g (cadr values))
         (b (caddr values)))
    (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))

(defun hexcolour-add-to-font-lock ()
  (interactive)
  (font-lock-add-keywords nil
   `((,(concat "#[0-9a-fA-F]\\{3\\}[0-9a-fA-F]\\{3\\}?\\|"
               (regexp-opt (x-defined-colors) 'words))
      (0 (let ((colour (match-string-no-properties 0)))
           (put-text-property
            (match-beginning 0) (match-end 0)
            'face `((:foreground ,(if (> 128.0 (hexcolour-luminance colour))
                                      "white" "black"))
                    (:background ,colour)))))))))

;; Whitespaces
(setq-default show-trailing-whitespace t)
(setq-default default-indicate-empty-lines t)
(setq-default indent-tabs-mode nil)

(set-face-bold-p 'font-lock-keyword-face t)
(set-face-italic-p 'font-lock-comment-face t)
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(show-paren-mode t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
