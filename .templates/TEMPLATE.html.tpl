<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="(>>>language_code<<<)" lang="(>>>language_code<<<)">
  <head>
    <title>(>>>title<<<)</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="(>>>USER_NAME<<<)" />
    <meta name="keywords" content="(>>>keywords<<<)" />
    <meta name="description" content="(>>>description<<<)" />
  </head>

  <body>
    <h1>(>>>title<<<)</h1>
    (>>>POINT<<<)
  </body>
</html>
>>>TEMPLATE-DEFINITION-SECTION<<<
("title" "Document Title: ")
("language_code" "Language Code: ")
("keywords" "Keywords: ")
("description" "Description: ")
