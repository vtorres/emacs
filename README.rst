
Usage
=====

* ln -s ~/emacs/.emacs ~/.emacs
* ln -s ~/emacs/.templates ~/.templates
* pip install jedi epc ipython rope ropemacs
* el-get-install [jedi|projectile|direx|ein|magit|autocomplete-css|web-mode|virtualenvwrapper|ropemacs|highlight-symbol|neotree]
